import random
import string


fm = {
      "ip"                  :   "10.210.20.70",
      "login"               :  "admin",
      "password"            :  "admin123A!",
      }


node = {
        "ip"              :     "10.150.100.61",
        "login"           :     "admin",
        "password"        :     "admin123A!"
        
        }


map_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(random.randint(10,20)))
tool_port = []
network_port = []
map  = {
        "alias"              : map_name,
        "toolPort-1"          :  "1/1/g1",
        "networkPort-1"       :  "1/1/g2",
        "collectorPort-1"     :  "1/1/x2",
        "toolPort-2"          :  "1/1/g1",
        "networkPort-2"       :  "1/1/g10",
        "collectorPort-2"     :  "1/1/g5",
        "toolPort-3"          :  "1/1/g1",
        "networkPort-3"       :  "1/1/g12",
        "collectorPort-3"     :  "1/1/g5",
        }




port_config = {
               "port_1"    : "1/1/g1",
               "port_2"    : "1/1/g3",
               "port_3"    : "1/1/g5",
               "port_4"    : "1/1/g7",
               "port_5"    : "1/1/g9",
               "port_6"    : "1/1/g11",
               "port_7"    : "1/1/g13",
               "port_8"    : "1/1/g15",
               "port_9"    : "1/1/x2",
               "port_10"   : "1/1/g2",
               "port_11"   : "1/1/g4",
               "port_12"   : "1/1/g6",
               "port_13"   : "1/1/g8",
               "port_14"   : "1/1/g10",
               "port_15"   : "1/1/g12",
               "port_16"   : "1/1/g14",
               "port_17"   : "1/1/g16"
               }




map_data = {
            "type"      :   "PassAll",
            "flowRules" :   "",
            ""          :   ""
            }



