
'''
-> Dont Touch This File If you dont know what are the fields.
-> Incidents will be reported if you Touch this file.
-> This File has no Return Values.
'''

import config
import random

#command = '''{'fromPorts' : ["4/1/g1"],'toPorts': ["4/1/g9"],'gsRules':{"drop":[],"pass":[]},'flowRules': {'drop':[],"pass":[]},'alias':'map_pass_all','type':'PassALL'}'''

def randomIPv4():
    return str(random.randint(1,255))+'.'+str(random.randint(1,255))+'.'+str(random.randint(1,255))+'.'+str(random.randint(1,255))

def randomMAC():
    mac = [ 0x00, 0x16, 0x3e,
        random.randint(0x00, 0x7f),
        random.randint(0x00, 0xff),
        random.randint(0x00, 0xff) ]
    return ':'.join(map(lambda x: "%02x" % x, mac))

def randomIPv6():
    M = 16**4
    return "2001:cafe:" + ":".join(("%x" % random.randint(0, M) for i in range(6)))


def randomHexOneByte():
    return random.randint(0x00,0xff)

vlan_min = random.randint(0,100)
vlan_max = random.randint(101,4096)
eth_type = ['0x0800','0x0806','0x86DD','0x8847','0x8870','0x8906','0x9000','0x8906','0x889A','0x8914','0x8100']


JsonPassAll = {'fromPorts' : [config.map['networkPort-1']],
               'toPorts': [config.map['toolPort-1']],
               'gsRules':{"drop":[],"pass":[]},
               'flowRules': {'drop':[],"pass":[]},
               'alias' : config.map['alias'],
               'type':'PassAll'}


JsonSharedCollector = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'collPorts': [config.map['collectorPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'Collector'
                       }
        
vlan_value = random.randint(0,4096)
        
JsonVlan_1  = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'toPorts': [config.map['toolPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'ByRule',
                       "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  str(vlan_value)},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "4096"}],
                                           "summary":"VLAN:Created"
                                           }]
                                 }
                       }
JsonVlan_2            = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'toPorts': [config.map['toolPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'ByRule',
                       "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  "78758"},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "199"}],
                                           "summary":"VLAN:100..199"
                                           }]
                                 }
                       }

JsonDropRule = {
                'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                            "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  "78758"},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "199"}],
                                           "summary":"VLAN:100..199"
                                           }],
                                "pass" : []
                                 }
             
               }


JsonIPv4PassDst = {
                'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipdst",
                                                        "value":  randomIPv4()},
                                                       ],
                                           "summary":"IP DST"
                                           }]
                                 }
                       }

JsonIPv4DropDst = {
                'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                                "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipdst",
                                                        "value":  randomIPv4()},
                                                       ],
                                           "summary":"IP DST DROP"
                                           }],
                                "pass" : []
                                 }
                       }

JsonIPv4PassSrc = {
                'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipsrc",
                                                        "value":  randomIPv4()},
                                                       ],
                                           "summary":"IP SRC PASS"
                                           }]
                                 }
                       }

JsonIPv4DropSrc = {
                'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                                "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipsrc",
                                                        "value":  randomIPv4()},
                                                       ],
                                           "summary":"IP  DROP"
                                           }],
                                "pass" : []
                                 }
                       }

JsonIPv4PassSrcDst = {'fromPorts' : [config.map['networkPort-1']],
                      'toPorts': [config.map['toolPort-1']],
                      'gsRules':{"drop":[],"pass":[]},
                      'flowRules': {'drop':[],"pass":[]},
                      'alias' : config.map['alias'],
                      'type':'ByRule',
                      "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipsrc",
                                                        "value":  randomIPv4()},
                                                       {"name" : "ipdst",
                                                        "value":  randomIPv4()
                                                        }
                                                       ],
                                           "summary":"IP PASS SRC-DST"
                                           }]
                                 }
                      }

JsonIPv4DropSrcDst = {'fromPorts' : [config.map['networkPort-1']],
                      'toPorts': [config.map['toolPort-1']],
                      'gsRules':{"drop":[],"pass":[]},
                      'flowRules': {'drop':[],"pass":[]},
                      'alias' : config.map['alias'],
                      'type':'ByRule',
                      "rules": {
                                "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipsrc",
                                                        "value":  randomIPv4()},
                                                       {"name" : "ipdst",
                                                        "value":  randomIPv4()
                                                        }
                                                       ],
                                           "summary":"IP DROP SRC-DST"
                                           }],
                                "pass" : []
                                 }
                      }

JsonMACValid  = {
               'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                          "drop" : [{
                                    "ruleId" : "1",
                                    "bidi"   : True,
                                    "props"  : [{
                                                "name" : "macsrc",
                                                "value":  str(randomMAC())},
                                                {"name" : "macdst",
                                                "value":  str(randomMAC())
                                                }
                                                ],
                                           "summary":"MAC SRC and DST"
                                           }],
                                "pass" : []
                                 }
               
               }

JsonMACInValid  = {
               'fromPorts' : [config.map['networkPort-1']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                          "drop" : [{
                                    "ruleId" : "1",
                                    "bidi"   : True,
                                    "props"  : [{
                                                "name" : "macsrc",
                                                "value":  "1.2.2.1.wa"},
                                                {"name" : "macdst",
                                                "value":  "3.4.5.62.@#$"
                                                }
                                                ],
                                           "summary":"MAC SRC and DST"
                                           }],
                                "pass" : []
                                 }
               
               }


JsonVlan_3            = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'toPorts': [config.map['toolPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'ByRule',
                       "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  str(vlan_min)},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  str(vlan_max)}],
                                           "summary":"VLAN:100..199"
                                           }]
                                 }
                       }

JsonVlan_4  = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'toPorts': [config.map['toolPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'ByRule',
                       "rules": {
                                "drop" : [ {
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  "588"},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "700"}],
                                           "summary":"VLAN:200.299"
                                           }],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  str(vlan_min)},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  str(vlan_max)}],
                                           "summary":"VLAN:100..199"
                                           },
                                          {
                                           "ruleId" : "2",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  "100"},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "199"}],
                                           "summary":"VLAN:200.299"
                                           }]
                                 }
                       }


JsonEtherTypes = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'toPorts': [config.map['toolPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'ByRule',
                       "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ethertype",
                                                        "value":  random.choice(eth_type)},
                                                       {
                                                        "name"  : "ethertype",
                                                        "value" :  random.choice(eth_type)}],
                                           "summary":"Ether Type "
                                           }]
                                 }
                       }



JsonIPVersiontypes = {'fromPorts' : [config.map['networkPort-1']],
               'toPorts': [config.map['toolPort-1']],
               'gsRules':{"drop":[],"pass":[]},
               'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                          "drop" : [],
                           "pass" : [{
                           "ruleId" : "1",
                            "bidi"   : True,
                            "props"  : [{
                                       "name" : "ipver",
                                       "value":  "6"},
                                        {
                                        "name"  : "ipver",
                                        "value" : "4"}],
                                         "summary":"IP VERSION"
                                           }]
                                 }
               }




JsonIPFragment = {'fromPorts' : [config.map['networkPort-1']],
                  'toPorts': [config.map['toolPort-1']],
                  'gsRules':{"drop":[],"pass":[]},
                  'flowRules': {'drop':[],"pass":[]},
                   'alias' : config.map['alias'],
                   'type':'ByRule',
                    "rules": {
                          "drop" : [],
                           "pass" : [{
                           "ruleId" : "1",
                            "bidi"   : True,
                            "props"  : [{
                                       "name" : "ipver",
                                       "value":  "v6"},
                                        {
                                        "name"  : "ipver",
                                        "value" : "v4"}],
                                         "summary":"IP VERSION"
                                           }]
                                 }
                     }



JsonTOS = {'fromPorts' : [config.map['networkPort-1']],
                  'toPorts': [config.map['toolPort-1']],
                  'gsRules':{"drop":[],"pass":[]},
                  'flowRules': {'drop':[],"pass":[]},
                   'alias' : config.map['alias'],
                   'type':'ByRule',
                    "rules": {
                          "drop" : [
                                    {
                           "ruleId" : "1",
                            "bidi"   : True,
                            "props"  : [{
                                       "name" : "tosval",
                                       "value":  "0xA1"},
                                        ],
                                         "summary":"TOS SERVICE"
                                           }
                                    ]
                                 }
                     }

JsonTTL = {       'fromPorts' : [config.map['networkPort-1']],
                  'toPorts': [config.map['toolPort-1']],
                  'gsRules':{"drop":[],"pass":[]},
                  'flowRules': {'drop':[],"pass":[]},
                   'alias' : config.map['alias'],
                   'type':'ByRule',
                    "rules": {
                          "drop" : [
                                    {
                           "ruleId" : "1",
                            "bidi"   : True,
                            "props"  : [{
                                       "name" : "ttl",
                                       "value":  "12"},
                                        {
                                        "name"  : "ttlmax",
                                        "value" : "7895124454"}],
                                         "summary":"TTL Service"
                                           }
                                    ]
                                 }
                     }


JsonIPv6DropSrcDst = {'fromPorts' : [config.map['networkPort-1']],
                      'toPorts': [config.map['toolPort-1']],
                      'gsRules':{"drop":[],"pass":[]},
                      'flowRules': {'drop':[],"pass":[]},
                      'alias' : config.map['alias'],
                      'type':'ByRule',
                      "rules": {
                                "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ip6src",
                                                        "value":  randomIPv6()},
                                                       {"name" : "ip6dst",
                                                        "value":  randomIPv6()
                                                        }
                                                       ],
                                           "summary":"IP DROP SRC-DST"
                                           }],
                                "pass" : []
                                 }
                      }




JsonTCPtcl =          {
                       'fromPorts' : [config.map['networkPort-1']],
                      'toPorts': [config.map['toolPort-1']],
                      'gsRules':{"drop":[],"pass":[]},
                      'flowRules': {'drop':[],"pass":[]},
                      'alias' : config.map['alias'],
                      'type':'ByRule',
                      "rules": {
                                "drop" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "tcpctl",
                                                        "value":  "0x21"}
                                                       ],
                                           "summary":"TCPtcl Map"
                                           }],
                                "pass" : []
                                 }
                      }



JsonPortList = {
                'fromPorts' : [config.map['networkPort-1'],config.map['networkPort-2'],config.map['networkPort-3']],
                'toPorts': [config.map['toolPort-1']],
                'gsRules':{"drop":[],"pass":[]},
                'flowRules': {'drop':[],"pass":[]},
                'alias' : config.map['alias'],
                'type':'ByRule',
                "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipdst",
                                                        "value":  randomIPv4()},
                                                       ],
                                           "summary":"IP DST"
                                           }]
                                 }
                       }




JsonJoinedRules = {
                   'fromPorts' : [config.map['networkPort-1'],config.map['networkPort-2'],config.map['networkPort-3']],
                   'toPorts': [config.map['toolPort-1']],
                   'gsRules':{"drop":[],"pass":[]},
                   'flowRules': {'drop':[],"pass":[]},
                   'alias' : config.map['alias'],
                   'type':'ByRule',
                   "rules": {
                                "drop" : [],
                                "pass" : [{
                                           "ruleId" : "1",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "ipsrc",
                                                        "value":  randomIPv4()},
                                                       {
                                                        "name"  : "ipdst",
                                                        "value" :  randomIPv4()}],
                                           "summary":"IP version 4 Map"
                                           },
                                          {
                                           "ruleId" : "2",
                                           "bidi"   : True,
                                           "props"  : [{
                                                        "name" : "vlan",
                                                        "value":  "100"},
                                                       {
                                                        "name"  : "vlanmax",
                                                        "value" :  "199"}],
                                           "summary":"VLAN:200.299"
                                           }]
                   
                   }
                   }

JsonSingleNetrokPort = {
                       'fromPorts' : [config.map['networkPort-1']],
                       'collPorts': [config.map['collectorPort-1']],
                       'gsRules':{"drop":[],"pass":[]},
                       'flowRules': {'drop':[],"pass":[]},
                       'alias' : config.map['alias'],
                       'type':'Collector'
                       }