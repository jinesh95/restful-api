import rest_module as rest,sys
from xml.etree import ElementTree
import os,time,random,config,json_creation
import json_port_operation as Json
import xml_parser as Parser
import clilib

clean = True
fm_ip = config.fm['ip']
fm_admin = config.fm['login']
fm_pass = config.fm['password']
hd_box = config.node['ip']
hd_usr = config.node['login']
hd_passwd = config.node['password']




def cleanup(ip,usrname,password):
    cmd = str('no map all')  
    login_hd = clilib.DevCon(ip,22,usrname,password,"ssh")
    login_hd.openConnection()
    login_hd.sendCommand(cmd,context='P_CONFIG').split('\n')
    login_hd.closeConnection()
    

def port_change_tool_network(*port_no):
    description = 'Verify The Given Port Configure as a Network Port'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_tool_to_network(port)
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        type = x.GetPortType(port)
        if type == 'network':
            print "Test Case Pass for " + description 
        else:
            print "Test Case Failed for " + description 

def port_change_network_tool(*port_no):
    description = 'Verify The Given Port Configure as a Tool Port'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_network_to_tool(str(port))
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        type = x.GetPortType(port)
        print type
        if type == 'tool':
            print "Test Case Pass for " + description + " for Port " + port
        else:
            sys.stderr.write("Test Case Failed for " + description + " for Port " + port)
        
def port_admin_enable(*port_no):
    description = 'Verify The Given Port Can Admin Enable from FM'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_port_admin(str(port),'enable')
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        admin_status = x.GetPortAdminStatus(port)
#        print type
        if admin_status == 'enable':
            print "Test Case Pass for " + description + " for Port " + port
        else:
            sys.stderr.write("Test Case Failed for " + description + " for Port " + port)

def port_admin_disable(*port_no):
    description = 'Verify The Given Port Can Admin Disable from FM'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_port_admin(str(port),'disable')
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        admin_status = x.GetPortAdminStatus(port)
#        print type
        if admin_status == 'disable':
            print "Test Case Pass for " + description + " for Port " + port
        else:
            sys.stderr.write("Test Case Failed for " + description + " for Port " + port)
        

def port_change_force_link_off(*port_no):
    description = 'Verify The Given Port Can Force Link Off from FM'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_force_link(str(port),False)
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        link_status = x.GetForceLinkStatus(port)
#        print link_status
        if link_status == 'false':
            print "Test Case Pass for " + description + " for Port " + port
        else:
            sys.stderr.write("Test Case Failed for " + description + " for Port " + port)
    

def port_change_force_link_on(*port_no):
    description = 'Verify The Given Port Can Force Link ON from FM'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    for port in port_no:
        data = Json.json_force_link(str(port),True)
        print data
        time.sleep(1)
        r.PutRequest('/fm/config/port/'+hd_box,200,data)
        
        result = r.GetRequest('/fm/config/port/'+hd_box,200,response='xml')
#    print result
        f = open('port_data.xml','w')
        for i in result:
            f.write(i)
        f.close()
        x = Parser.XmlParser('port_data.xml')
        link_status = x.GetForceLinkStatus(port)
        print link_status
        if link_status == 'true':
            print "Test Case Pass for " + description + " for Port " + port
        else:
            sys.stderr.write("Test Case Failed for " + description + " for Port " + port)
        

#def port_change_
    
    
if __name__== '__main__':
    
#     port_change_tool_network(config.port_config['port_1'],config.port_config['port_2'],config.port_config['port_3'])
#     port_change_network_tool(config.port_config['port_1'],config.port_config['port_2'],config.port_config['port_3'],config.port_config['port_4'])
#     port_admin_enable(config.port_config['port_1'])
#     port_admin_disable(config.port_config['port_1'])
#     port_change_force_link_off(config.port_config['port_1'])
#     port_change_force_link_on(config.port_config['port_1'],config.port_config['port_2'],config.port_config['port_3'])
     
    
    