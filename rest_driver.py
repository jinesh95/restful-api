import rest_module as rest,sys
from xml.etree import ElementTree
import os,time,random,config,json_creation
import json_creation as Json
import xml_parser as Parser
import clilib

clean = True
fm_ip = config.fm['ip']
fm_admin = config.fm['login']
fm_pass = config.fm['password']
hd_box = config.node['ip']
hd_usr = config.node['login']
hd_passwd = config.node['password']

def cleanup(ip,usrname,password):
    cmd = str('no map all')  
    login_hd = clilib.DevCon(ip,22,usrname,password,"ssh")
    login_hd.openConnection()
    login_hd.sendCommand(cmd,context='P_CONFIG').split('\n')
    login_hd.closeConnection()
    


def Test_Create_PassAll_Map():
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonPassAll
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)   

def Test_Create_Shared_Collector_Map():
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonSharedCollector
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)   
      

def Test_Create_VLAN_Map():
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonVlan_2
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)   
    
def Test_Create_VLAN_DROP_rule_Map():
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonDropRule
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)   
       
def Test_Case_1():
    description = 'Drop Valid IPv4 Destination Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4DropDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True:cleanup(hd_box,hd_usr,hd_passwd)      

def Test_Case_2():
    description = 'Pass Valid IPv4 Destination Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4PassDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True:cleanup(hd_box,hd_usr,hd_passwd) 

def Test_Case_3():
    description = 'Drop Valid IPv4 Source Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4DropSrc
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True:cleanup(hd_box,hd_usr,hd_passwd) 

def Test_Case_4():
    description = 'Pass Valid IPv4 Source Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4PassSrc
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True:cleanup(hd_box,hd_usr,hd_passwd)


def Test_Case_5():
    description = 'Pass Valid IPv4 Source-Destination Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4PassSrcDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)  
    
def Test_Case_6():
    description = 'Drop Valid IPv4 Source-Destination Filter'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4DropSrcDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed\n\n\n")
    if clean == True:  cleanup(hd_box,hd_usr,hd_passwd)      

def Test_Case_CM_2256():
    description = 'Test Map Creation with VLANS'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonVlan_1
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stdout.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)      
     
    



def Test_Case_CM_2257():
    description = 'Test Different types of MAC source/Destination address in Map rules from FM UI'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonMACValid
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    
def Test_Case_CM_2258():
    description = 'MAP Rules with INVALID VLAN ID'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonVlan_2
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True:  cleanup(hd_box,hd_usr,hd_passwd)

def Test_Case_CM_2350():
    description = 'Verify The Map Creation with different VLAN Ranges'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonVlan_3
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True:  cleanup(hd_box,hd_usr,hd_passwd)


def Test_Case_CM_2351():
    description = 'Verify The Map creation with multiple VLAN Ranges'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonVlan_4
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)



def Test_Case_CM_2352():
    description = 'Verify Map Creation with multiple ether types'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonEtherTypes
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)

def Test_Case_CM_2354():
    description = 'Verify The Map Created with different IPVer Types'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPVersiontypes
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)

def Test_Case_CM_2356():
    description = 'Verify The Map Created with different TOS Values'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonTOS
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    
    
def Test_Case_CM_2358():
    description = 'Verify The Map Created with different TTL Values'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonTTL
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    


def Test_Case_CM_2359():
    description = 'Verify The Map Created with different Source/Destination MACs'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonMACValid
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)





def Test_Case_CM_2361():
    description = 'Verify The Map Craetion with Different IPv4 Address'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv4PassDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)



def Test_Case_CM_2362():
    description = 'Verify The Map Craetion with Different IPv6 Address'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonIPv6DropSrcDst
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)

def Test_Case_CM_2363():
    description = 'Verify The Map with different types of tcpctl'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonTCPtcl
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    


def Test_Case_CM_2368():
    description = 'Verify The Delete Differnet Rules on the Map'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonTCPtcl
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    
    
def Test_Case_CM_2226():
    description = 'Verify The Creation of map with different network port group'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonPortList 
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    

def Test_Case_CM_2237():
    description = 'Verify The Creation of map with Joined Rules'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonJoinedRules 
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    else:
        sys.stderr.write("Test Case Failed "+str(description)+'\n\n\n')
#    if clean == True: cleanup(hd_box,hd_usr,hd_passwd)
    

def Test_Case_CM_2443():
    description = 'Verify Addition of shared collector to a map with a single network port'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    data  = Json.JsonSingleNetrokPort
    r.PostRequest('/fm/config/afm/map/'+hd_box,201,data)
    result = r.GetRequest('/fm/config/afm/map/'+hd_box,200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
    if clean == True:  r.DeleteRequest('/fm/config/afm/map/'+hd_box+'/'+config.map['alias'],204,response='xml')



def Test_Case_VMM_1():
    description = 'Checked vmm vcenter list'
    r = rest.RestApi(fm_ip,fm_admin,fm_pass)
    result = r.GetRequest('/vmm/vcenter',200,response='xml')
    f = open('test1.xml','w')
    for i in result:
        f.write(i)
    f.close()
    x = Parser.XmlParser('test1.xml')
    if config.map['alias'] in x.GetMapName():
        sys.stdout.write("Test Case Pass\n\n\n")
#    if clean == True:  r.DeleteRequest('/fm/config/afm/map/'+hd_box+'/'+config.map['alias'],204,response='xml')



if __name__ =='__main__':
      Test_Create_PassAll_Map()
      time.sleep(3)
      Test_Create_Shared_Collector_Map()
      time.sleep(3)
      Test_Create_VLAN_Map()
      time.sleep(3)
      Test_Create_VLAN_DROP_rule_Map()
      time.sleep(3) 
      Test_Case_1()
      time.sleep(3) 
      Test_Case_2()
      time.sleep(3)
      Test_Case_3()
      time.sleep(3)
      Test_Case_4()
      time.sleep(3)
      Test_Case_5()
      time.sleep(3)
      Test_Case_6()
      time.sleep(3)
      Test_Case_CM_2256()
      time.sleep(3)
      Test_Case_CM_2257()
      time.sleep(3)
      Test_Case_CM_2258()
      time.sleep(3)
      Test_Case_CM_2350()
      time.sleep(3)
      Test_Case_CM_2351()
      time.sleep(3)
      Test_Case_CM_2352()
      time.sleep(3)
      Test_Case_CM_2354()
      time.sleep(3)
      Test_Case_CM_2356()
      time.sleep(3)
      Test_Case_CM_2358()
      time.sleep(3)
      Test_Case_CM_2359()
      time.sleep(3)
      Test_Case_CM_2361()
      time.sleep(3)
      Test_Case_CM_2362()
      Test_Case_CM_2363()
      Test_Case_CM_2226()
      Test_Case_CM_2237()    
      Test_Case_CM_2443()
#     Test_Case_VMM_1()  
    


