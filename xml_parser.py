'''
Customized Parser For parse and return from Gigamon XML file for H Series Device.
@author: Jinesh Patel
'''

from xml.etree import ElementTree
import os,time,random




class XmlParser():
    
    def __init__(self,xml_file_name):
        self.tree = ElementTree.parse(xml_file_name)
        self.root = self.tree.getroot()
        
    def GetMapName(self):
        self.map_name = []
        for self.child in self.root.iter('afmMap'):
            self.map_name.append(self.child.attrib.values()[0])
        if self.map_name == []:
            print "No Map Configured\t\n\n"
        return self.map_name
    
    def GetDeviceIp(self):      
        self.device_ip = []
        for self.child in self.root.iter('deviceIp'):
            self.device_ip.append(self.child.text)
        if self.device_ip == []:
            print "No Device Associate with this FM\t\n\n"
        return self.device_ip
        
    def GetNetworkPorts(self):
        self.map_name = self.GetMapName()
        self.net_port = []
        for nport in self.root.iter('fromPorts'):
            self.net_port_ = nport.getchildren()
            print self.net_port_[0].text
      
          
    def GetPortAdminStatus(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        admin = gigaPort.find('adminStatus').text
                    except AttributeError:
                        return None
                    port_data.append([port,admin])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
     
    def GetPortType(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        type = gigaPort.find('portType').text
                    except AttributeError:
                        return None
                    port_data.append([port,type])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]       
     
    def GetPortLinkStatus(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        link_status = gigaPort.find('operStatus').text
                    except AttributeError:
                        return None
                    port_data.append([port,link_status])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
               
    def GetConfigSpeed(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        config_speed = gigaPort.find('configSpeed')
                    except AttributeError:
                        return None
                    port_data.append([port,config_speed])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
    
    def GetOperSpeed(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        operation_speed = gigaPort.find('operSpeed')
                    except AttributeError:
                        return None
                    port_data.append([port,operation_speed])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
             
    def GetDuplex(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        duplex = gigaPort.find('duplex').text
                    except AttributeError:
                        return None
                    port_data.append([port,duplex])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
    
    
    def GetForceLinkStatus(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        force_link_status = gigaPort.find('forceLinkUpEnabled').text
                    except AttributeError:
                        return None
                    port_data.append([port,force_link_status])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]
    
    def GetAutoNegStatus(self,port_ip):
        for self.child in self.root:
                port_data = []
                for gigaPort in self.root.findall('gigaPort'):
                    port = gigaPort.get('portId')
                    try:
                        auto_neg_status = gigaPort.find('autoNegEnabled').text
                    except AttributeError:
                        return None
                    port_data.append([port,auto_neg_status])
                for i in range(len(port_data)):
                    if port_data[i][0] == str(port_ip):
                        return port_data[i][1]

    

if __name__ == '__main__':
    x = XmlParser('port_data.xml')
    print x.GetDuplex('1/1/g1')