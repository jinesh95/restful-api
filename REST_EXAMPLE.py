import json
import httplib
from xml.etree.ElementTree import ElementTree,SubElement,tostring,Element
import string
import base64
import sys

class RestReqGen():
    
    def __init__(self,Req_type,systemip,username,password,url=None):
        if type(username) and type(password) != str:
            sys.stderr.write("Check Your Username or Password!!! May be not in proper format\n\n\n")
            sys.exit(-1)
        else:    
            self.auth = string.strip(base64.encodestring(username+':'+password))
            self.conn = httplib.HTTPSConnection(systemip)
            self.url = url 
        
    def GetRequest(self):
        self.conn.putrequest("GET",self.url)
        self.conn.putheader("Authorization","Basic %s"%self.auth)
        self.conn.putheader("Accept","application/xml")
        self.conn.endheaders()
        return self.conn.getresponse()
    
    def PutRequest(self):
        self.conn.putrequest("PUT",self.url)
        self.xml = XmlGen("devCred","deviceIp","httpUserName","httpPassword")
        
    def XmlGen(self,root,*child1):
        self.root = Element(str(root))
        for i in child1:
            self.child = SubElement(self.root,)
            
            
        
        
        
        
        
        
if __name__ == '__main__':
    rest = RestReqGen('GET','10.210.20.20','admin','admin123A!','/api/0.9/sys/info')
    a = rest.GetRequest()
    print a.status
    print a.read()

'''
200
{"version": {"ts": "131217_0114", "ver": "2.1"}, "systemTime": {"utc": "1387430926611", "text": "2013-12-19T05:28:46Z"}, "instanceId": "0050569A69AB"}


or 

200
<systemInfo><version><ts>131217_0114</ts><ver>2.1</ver></version><systemTime><utc>1387430959257</utc><text>2013-12-19T05:29:19Z</text></systemTime><instanceId>0050569A69AB</instanceId></systemInfo>

'''        
        
