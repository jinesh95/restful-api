import httplib
import json
import string
import base64
import sys,os
from xml.etree import ElementTree
#import xml_genrator as xml_gen


debug = True
api_version = '/api/0.9'



class RestApi():
    
    def __init__(self,fm_ip,user,password):
        protocol ='https'
        self.user = user
        self.password = password
        self.protocol = protocol 
        self.auth = string.strip(base64.encodestring(self.user+':'+self.password))
        if self.protocol == 'https':
            self.connection = httplib.HTTPSConnection(fm_ip)
        else:
            self.connection = httplib.HTTPConnection(fm_ip)
            
    
    def GetRequest(self,url,status_code,response='xml'):
        self.url = api_version+url
        self.status_code = status_code
        con = self.connection
        self.response = response
        con.putrequest("GET",self.url)
        con.putheader("Authorization","Basic %s"%self.auth)
        if self.response == 'json':
            con.putheader("Accept","application/json")
        else:
            pass
        con.endheaders()
        res = con.getresponse()
        if debug : print res.status
        if debug : print status_code
        if res.status == status_code:
            if debug : print "Status Code is Expceted.....\n"
            return res.read()
            #return res.read()
        else:
            sys.stderr.write("Status Code Does not matched...Please Checked....\n\n\n")
            sys.stderr.write("ERROR: Please Check REST API "+ str(url)+" And GET Method\n\n\n")
             


        
    def PostRequest(self,url,status_code,data):
        self.url = api_version+url
        self.status_code = status_code
        con = self.connection
        self.response = "xml"
        command = data
        payload =  json.dumps(command,ensure_ascii=False).encode('UTF-8')
        headers = {'Authorization':"Basic %s"%self.auth,"Accept":"application/json","Content-type":"application/json"}
        con.request('POST',self.url,payload,headers)
        res = con.getresponse()
        if debug : print res.status
        if debug : print res.read()
        if debug : print status_code
        if res.status == status_code:
            if debug : print "Status Code is Expceted.....\n"
            return res.read()
        else:
            sys.stderr.write("Status Code Does not matched...Please Checked....\n\n\n")
            sys.stderr.write("ERROR: Please Check REST API "+ str(url)+" And POST Method\n\n\n")
            return res.read()
    
    
    def PutRequest(self,url,status_code,data):
        self.url = api_version+url
        self.status_code = status_code
        con = self.connection
        self.response = "xml"
        command = data
        payload =  json.dumps(command,ensure_ascii=False).encode('UTF-8')
        headers = {'Authorization':"Basic %s"%self.auth,"Accept":"application/json","Content-type":"application/json"}
        con.request('PUT',self.url,payload,headers)
        res = con.getresponse()
        if debug : print res.status
        if debug : print res.read()
        if debug : print status_code
        if res.status == status_code:
            if debug : print "Status Code is Expceted.....\n"
            return res.read()
        else:
            sys.stderr.write("Status Code Does not matched...Please Checked....\n\n\n")
            sys.stderr.write("ERROR: Please Check REST API "+ str(url)+" And PUT Method\n\n\n")
            return res.read()
        
       
    def DeleteRequest(self,url,status_code,response='xml'):
        self.url = api_version+url
        self.status_code = status_code
        con = self.connection
        self.response = response
        con.putrequest("DELETE",self.url)
        con.putheader("Authorization","Basic %s"%self.auth)
        if self.response == 'json':
            con.putheader("Accept","application/json")
        else:
            pass
        con.endheaders()
        res = con.getresponse()
        if debug : print res.status
        if debug : print status_code
        if res.status == status_code:
            if debug : print "Status Code is Expceted.....\n"
            return res.read()
            #return res.read()
        else:
            sys.stderr.write("Status Code Does not matched...Please Checked....\n\n\n")
            sys.stderr.write("ERROR: Please Check REST API "+ str(url)+" And DELETE Method\n\n\n")
             
#         
#     