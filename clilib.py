import time
import re
import pexpect
import telnetlib
import commonlib


__version__ = '1.3'


use_plink = False
if use_plink:
    import plink


# Define prompts
#Gigamon HD: [root@autobox ~]# (centOS)
#Gigamon GigaVUE: GV2404-23>   (ecOS)

_reLogin          = re.compile ( r"login: $", re.I)
_rePassword    = re.compile ( r"(.*\'s password|Password): $")
_reConfirm        = re.compile ( r"\(YES\): $", re.I )
_reConfirm2       = re.compile ( r"Enter \'YES\' to confirm this operation:\s*$", re.I )
_reConfirm3    = re.compile (r"-\s+continue\s+\(y\/n\)\?\s*$")
_reConfirm4 = re.compile (r"Store key in cache\?\s+\(y\/n\)", re.I)
#_reConfirmSSH     = re.compile ( r"\(yes\/no\)\? $", re.I )
#_reStandardPrompt        = re.compile ( r"[^\-][a-zA-z\-0-9_\[\] ]{2,64}[\t ]*>[\t ]*$", re.I )
_reStandardPrompt        = re.compile ( r"[a-zA-Z\-0-9_\[\]\* :]{2,64}[\t ]*>[\t ]*$", re.I )
_reEnablePrompt        = re.compile ( r"[a-zA-Z\-0-9_\[\]\* :]{2,64}[\t ]*#[\t ]*$", re.I )
_reConfigPrompt   = re.compile ( r"[a-zA-Z\-0-9_\[\]\* :]{2,64}[\t ]*\(config\s*.*\)[\t ]*#[\t ]*$", re.I )
_reScrollPrompt_end = re.compile ( r"lines\s+\d+-\d+\/\d*\s+\([END]+\).*", re.S)
_reScrollPrompt = re.compile ( r"lines\s+\d+-\d+.*", re.S)
_reAll = re.compile(".*", re.S)

promptList = ('P_STANDARD','P_ENABLE','P_CONFIG', 'P_EXIT')



class DevCon:
    """
    SSH/Telnet Library for Gigatest framework
    Purpose: executing GV and HD CLI commands over an SSH or Telnet connection.
    
    """

    def __init__ (self, ipaddress, port=None, username=None, password=None, cliaccess='ssh', idle=0):
        '''Initialize connection object for command line access
        Arguments:
            address - IP address of device
            username - user name
            password - pass word
            cliAccess - how to connect, valid values: "serial", "telnet", "ssh" (defaults to ssh)
            idle - wait for the specified number of seconds to catch any extra output after sending each CLI command. Default is 0
            Note: This parameter can be overwritten by specifying diff. idle value in 'sendCommand'   
        '''

        if use_plink:
            self.PlinkConsole = plink.DevCon(ipaddress, port, username, password, cliaccess, idle)
            return

        self.cliAccess = cliaccess
        self.ipaddress = ipaddress
        self.idle = idle
        if self.ipaddress == None:
            raise Exception ('** DevCon: No IP address to connect to the device specified')
        self.port = port
        if not self.port:
            if self.cliAccess == 'ssh':
                self.port = 22
            elif self.cliAccess == 'telnet':
                self.port = 23    
                
        self.username = username
        self.password = password
        self.context        = None
        self.serialAddress  = None
        self.serialPort     = None

    
    def openConnection ( self, loginTimeout=60, commandTimeout=120):
        '''Open connection to device and parse though all login username/password prompts

        loginTimeout - log in timeout
        promptTimeout - timeout to get an expected prompt
        commandTimeout - timeout to get a response from cli command
        '''

        if use_plink:
            return self.PlinkConsole.openConnection(loginTimeout, commandTimeout)

        self.loginTimeout   = loginTimeout
        self.commandTimeout = commandTimeout
        self.context        = None
        self.serialAddress  = None
        self.serialPort     = None
        

        
        if self.cliAccess.find('serial') >= 0: 
            self.serialAddress = self.ipaddress
            self.serialPort = self.port
        
        if self.cliAccess not in ('serial', 'telnet', 'ssh'):
            raise Exception ('** DevCon: Invalid access method: {0}'.format(self.cliAccess) )
        
        if self.cliAccess == 'ssh':
            #cmd =  'plink -ssh {0}@{1} -pw {2}'.format(self.username, self.ipaddress, self.password)
            cmd = 'ssh -q -o UserKnownHostsFile=/dev/null -o stricthostkeychecking=no {0}@{1}'.format (self.username, self.ipaddress)
        else:
            #cmd =  'plink -telnet {0}@{1}'.format(self.username, self.ipaddress)
            cmd = 'telnet -l {0} {1}'.format (self.username, self.ipaddress)
        self.connector = pexpect.spawn (cmd)
        self.connector.setecho (False)
        _reHWver = re.compile('H\s+Series', re.I)
        self.platform = None
        while 1:
            result = self.connector.expect ( [_reConfirm4, _reLogin, _rePassword, _reHWver, _reStandardPrompt, 
                                            pexpect.EOF, pexpect.TIMEOUT], timeout=self.loginTimeout)
            if result == 0: self.connector.send ('n\r')                    # ssh known host prompt
            if result == 1: self.connector.send (self.username+"\r")        # username prompt
            if result == 2: self.connector.send (self.password+"\r")        # password prompt
            if result == 3: self.platform='h'
            if result == 4:                                                    # caught command prompt
                self.context = 'P_STANDARD'
                break
            if result == 5:                                                    # EOF error
                raise Exception ('Telnet/SSH unable to login')
            if result == 6:
                raise Exception ('Telnet/SSH timeout on login')
            #print self.connector.before+self.connector.after
        if self.platform == 'h':
            cmdLine = 'enable\nconfig t\ncli session auto-logout 0\ncli session terminal type dump\ncli session terminal length 350\ncli session terminal width 240\nexit\ndisable\n'
            #for cmd in cmdLine:
            #print cmdLine
            self.connector.send(cmdLine)
            result = self.connector.expect([_reStandardPrompt, pexpect.EOF, pexpect.TIMEOUT], timeout=5)
            if result == 1:                                                    # EOF error
                raise Exception ('SSH/Telnet: unable to configure terminal session  -- EOF')
            if result == 2:
                raise Exception ('SSH/Telnet: unable to configure terminal session -- timeout')
            #print self.connector.before+self.connector.after
            #if result > 0: raise Exception ('Unexpected prompt after configuring SSH/Telnet terminal: {0}'.format(self.connector.before+self.connector.after))
            #print self.connector.before+self.connector.after
#             print self.connector._sndcmd ('enable')
#             print self.connector._sndcmd ('config t')
#             print self.connector._sndcmd ('cli session auto-logout 0')
#             print self.connector._sndcmd ('cli session terminal type dump')
#             print self.connector._sndcmd ('cli session terminal length 350')
#             print self.connector._sndcmd ('cli session terminal width 120')
#             print self.connector._sndcmd ('exit')
#             print self.connector._sndcmd ('exit')
#
    def sendCommand (self, cmdLine, context=None, exp_prompt=None, timeout=None, password='', error_regex=None, confirm=True, idle=-1):
        '''Send a CLI command to a device
        cmdLine - CLI command, a return is always added
        timeout - how long before forcing a timeout error (default use initial values)
        context - modal context state to use. None=Use current context
        Four context states:
            P_STANDARD   - post log in mininum configuration and view
            P_ENABLE - base configuration and view
            P_CONFIG - system configuration context
            P_CUSTOM - (internal use only) non-standard context state. Indicates that 'exp_prompt' was specified for previous command   
            P_EXIT   - exit command line and drop connection
        exp_prompt - expect user defined prompt after sending command
        password - password string to send if CLI asks for any extra password to enter during a command execution
        error_regex - if specified, check command output for any error matching the specified regular expression.
        Default is None - do not check output. 
        Example: error_regex='\%\s+\w+.*' # raise an exception (stop the script) if any error message that starts from '%' is in output  
        confirm - If True, send 'yes' if CLI asks to confirm an operation, and send 'no' otherwise.
        idle - wait for 'idle' seconds to catch an extra output (leftover) after catching an expected prompt. 
        Default is to use the global 'idle' defined in the class constructor
           
        
          

        '''

        if use_plink:
            return self.PlinkConsole.sendCommand(cmdLine, context, exp_prompt, timeout, password, error_regex, confirm, idle)

        if idle < 0: idle = self.idle
        if timeout == None: timeout = self.commandTimeout
        if self.connector == None: raise Exception ("sendCommand: Connection socket is not opened")

        # -- Match command context (prompt) to current system context level
        if context and context not in promptList:
            raise Exception ('Invalid context {0} specified'.format(context))
        if self.context == 'P_CUSTOM':
            #self._sndcmd ('', timeout=timeout, password=password)
            context = self.context
        while context and context != self.context: # Switch from current context (self.context) to expected one (context)
            if self.context == 'P_STANDARD' and context in ('P_ENABLE', 'P_CONFIG'):
                self._sndcmd ('enable', context=context, timeout=timeout, password=password)
                
            elif self.context == 'P_ENABLE' and context in ('P_CONFIG',):
                self._sndcmd ('configure terminal', context=context, timeout=timeout, password=password)
            
            elif self.context == 'P_CONFIG' and context in ('P_ENABLE',):
                self._sndcmd ('exit', context=context, timeout=timeout, password=password)
                time.sleep(1)
            
            elif self.context == 'P_ENABLE' and context in ('P_STANDARD',):
                self._sndcmd ('disable', context=context, timeout=timeout, password=password)
                time.sleep(1)
            
            elif self.context in ('P_STANDARD', 'P_ENABLE') and context in ('P_EXIT'):
                break
            else:
                self._sndcmd ('exit', context=context, timeout=timeout, password=password)
                time.sleep(1)
    
    # -- Now send the command for this context
        output = self._sndcmd (cmdLine, context=context, exp_prompt=exp_prompt, timeout=timeout, password=password, confirm=confirm, idle=idle)
        commonlib.glog('CLI', output)
        if error_regex:
            re_obj = re.search('(%s)'%error_regex, output, re.I) 
            if re_obj:
                raise Exception ('"{0}" command output error: {1}'.format(cmdLine, re_obj.group(1)))

        return output
        
    def sendBatchOfCommands (self, *commands):
        '''Send a batch of CLI commands to a device
        Arguments: commands - list of CLI commands to send
        Return: output from all commands executed in the batch
        Notes:
            - All commands get executed from a current prompt. If the commands in the batch has to be executed from a specific context,
             it has to be set before calling 'sendOfBatchCommands' method
        '''
        output = ''
        for command in commands:
            output += self.sendCommand (command) 
        return output


    # --------------------------------------------------------------------------------

    def _sndcmd (self, cmdLine, context=None, exp_prompt=None, timeout=None, password='', confirm=True, idle=0):
        '''Internal command routine. Walk up/down the command contextual states.'''
        
        if not self.connector:
            raise Exception ("Connection closed")
        if self.cliAccess in ('ssh', 'telnet'):
            reIndex = self.connector.expect ([_reAll, pexpect.EOF, pexpect.TIMEOUT], timeout=0.1)
            if reIndex == 0:
                commonlib.glog('debug',"#### Output leftover before running '{0}': >>>>> {1} <<<<<".format(cmdLine, str(self.connector.before)+str(self.connector.after)))
                #print "###leftover"+self.connector.before+self.connector.after
            # -- Send command and wait for command or confirm prompt
            self.connector.send (cmdLine+'\r')
            store = ""
            if context == 'P_EXIT':
                time.sleep(1)
                self.connector.close()
                #self.connector = None
                return ''
            expPrompts = [_reConfirm2, _reConfirm3]            
            if exp_prompt:
                _reExpPrompt = re.compile (exp_prompt)
                expPrompts.append(_reExpPrompt)
                #idx_sft = 2
            else:
                expPrompts.extend([_reStandardPrompt, _reEnablePrompt, _reConfigPrompt])
                #idx_sft = 0
            expPrompts.extend([_reScrollPrompt_end, _reScrollPrompt, 'password:\s*', pexpect.EOF, pexpect.TIMEOUT])
            npr = len(expPrompts)
            while 1:     
                reIndex = self.connector.expect ( expPrompts, timeout=timeout )
                
                match_before = str(self.connector.before)
                match_before = re.sub('\[J','', match_before)
                match_after = str(self.connector.after)
                match_after = re.sub('\[J','', match_after)
                #match_match = self.connector.match.group(0)
                # -- Strip out echoed command lines and line breaks                
                #store += re.sub(cmdLine+'\s','',match_before).strip()
                store +=  match_before + match_after #match_after
                #print store
                #store +=  re.sub('\r', '', match_before) + str(self.connector.after) #re.sub('\r','',self.connector.after)).strip()
                # -- Strip out echoed command lines and line breaks                
                #store += re.sub(cmdLine+'\s','',match_before).strip()+str(self.connector.after)
                
                if reIndex == npr-1: # timeout
                    raise Exception ('SSH connection timed out')
                
                elif reIndex == npr-2: #EOF
                    if exp_prompt:
                        self.context = 'P_CUSTOM'
                    else:
                        self.context = 'P_STANDARD'
                    self.connector.close ()
                    break
                elif reIndex == npr-3: #CLI password prompt
                    if password:
                        self.connector.send (password+'\r')
                    else:
                        continue
                elif reIndex == npr-4: #scrolling
                    self.connector.send ('z')
                    #continue
                elif reIndex == npr-5: #end of scrolling
                    self.connector.send ('\r')
                    time.sleep(1)
                    #continue
                elif reIndex == npr-6:
                    if exp_prompt:
                        self.context = 'P_CUSTOM'
                    else:
                        self.context = 'P_CONFIG'
                    break
                elif reIndex == npr-7:
                    if exp_prompt:
                        self.connector.send ('y\r')
                        continue
                    else:
                        self.context = 'P_ENABLE'
                        break
                elif reIndex == npr-8:
                    if exp_prompt:                        
                        if confirm:
                            self.connector.send ('YES\r')
                        else:
                            self.connector.send ('NO\r')
                        continue
                    else:
                        self.context = 'P_STANDARD'
                        break
                elif reIndex == npr-9:
                        self.connector.send ('y\r')
                        continue
                elif reIndex == npr-10:                    
                    if confirm:
                        self.connector.send ('YES\r')
                    else:
                        self.connector.send ('NO\r')
                    continue

                        

            #store = "".join(filter(lambda x: ord(x)<128, store))
            #store = re.sub("([^-_a-zA-Z0-9!@#%&=,/'\";:~`\$\^\*\(\)\+\[\]\.\{\}\|\?\<\>\\]+|[^\s]+)", "", store) #remove non-ASCI chars
            #store = re.sub("(\[\?1h=|\[24;1H|\[K|\[7m|\[27m|\[24;1H|\[\?1l>|\[m)", "", store)
            #store = re.sub("\[J","", store)
            #self.connector.before = None
            #self.connector.after = None
            #self.connector.expect ('-re $')
            #self.connector.sendeof()
            #print store
            if idle:
                reAll = re.compile(".*", re.S)
                reIndex = self.connector.expect ([reAll, pexpect.EOF, pexpect.TIMEOUT], timeout=int(idle))
                if reIndex == 0:
                   commonlib.glog('debug',"Output leftover after running '{0}': >>>>> {1} <<<<<".format(cmdLine, str(self.connector.before)+str(self.connector.after)))
            
            return store



    def getCardInfo_h(self, box_id, slot_id):
        if not box_id:
            raise Exception ("No box id specified")
        if not slot_id:
            raise Exception ("No slot id specified")
        out = self.sendCommand ("show card box-id {0}".format(box_id), context='P_CONFIG')
        if out.startswith ('\%'):
            raise Exception ("Cannot get card info: {0}".format(out))
        box_lst = out.split('Box ID:')
        for box_info in box_lst:
            box_info = box_info.strip()
            if re.match('^%s'%box_id, box_info):
                break
            box_info = None
        if not box_info:
            raise Exception ("No card info for box {0} found ".format(box_id))
        card_lst = box_info.split('\n')
        card_params={}
        for card in card_lst:
            re_card_info = re.search('^%s\s+([a-zA-Z0-9_\-]+)\s+([a-zA-Z0-9_\-]+)\s+([a-zA-Z0-9_\-/]+)\s+([a-zA-Z0-9_\-]+)\s+([a-zA-Z0-9_\-]+)\s+([a-zA-Z0-9_\-.]+)\s+'%slot_id, card)
            if re_card_info:
                card_params['config'] = re_card_info.group(1)
                card_params['oper_status'] = re_card_info.group(2)
                card_params['hw_type'] = re_card_info.group(3)
                card_params['product_code'] = re_card_info.group(4)
                card_params['serial_num'] = re_card_info.group(5)
                card_params['hw_rev'] = re_card_info.group(6)
                break
        return card_params
    
    def check_card_up_h (self, box_id, slot_id, timeout=60):
        ''' Check if a given card is up. If it's not up, re-check within 'timeout' sec.'''
        retr = int(timeout/10)
        for i in range(retr):
            card_params = self.getCardInfo_h(box_id, slot_id)
            if card_params['oper_status'].find('up') >= 0: break
            if i == retr:
                raise Exception ('The card in slot {0} is not up'.format(slot_id))
            time.sleep(10)
    def config_term_h (self, term_type='console', term_len = 350, term_width = 120 ):
        self.sendCommand('no cli session auto-logout', context='P_CONFIG')
        self.sendCommand('cli session auto-logout 0', context='P_CONFIG')
        self.sendCommand('cli session terminal type {0}'.format(term_type), context='P_CONFIG')
        self.sendCommand('cli session terminal length {0}'.format(term_len), context='P_CONFIG')
        self.sendCommand('cli session terminal width {0}'.format(term_width), context='P_CONFIG')
        
    def h_getBoxId(self):
        output = self.sendCommand ('show chassis')
        bid_obj = re.search('Box\s+ID\s*:\s*(\d+)', output)
        if bid_obj:
            return bid_obj.group(1)
        return None
            
        # hw_type=None, product_code=None, serial_num=None, hw_ver=None, conf_status='configured', oper_status='up'
    def closeConnection (self):
        '''Close the connector'''

        if use_plink:
            return self.PlinkConsole.closeConnection()

        if self.connector:
            self.connector.close ()



if __name__ == '__main__':
    deviceParams1 = {
                    'ipaddress': '10.150.52.16',
                    'port': '22',
                    'username': 'admin',
                    'password': 'admin',
                    'cliaccess': 'ssh'
                    }
    
    deviceParams2 = {
                    'ipaddress': '192.168.1.23',
                    'port': '22',
                    'username': 'root',
                    'password': 'root123',
                    'cliaccess': 'ssh'
                    }
    
    
    
    #hdboxObj = DevCon('192.168.0.31', 22, 'root', 'root123', 'ssh')
    #hdboxObj.openConnection()
    #print "waiting for "
    #output = hdboxObj.sendCommand ('config system remote_timeout 10', exp_prompt='SSH session timeout.  Connection closed.')
    #print output
    
    
    #hdboxObj = DevCon(deviceParams1['ipaddress'], deviceParams1['port'], deviceParams1['username'], deviceParams1['password'], deviceParams1['cliaccess'])
    #hdboxObj.openConnection()
    
    #print hdboxObj.h_getBoxId()
    
    
    #gvboxObj = DevCon('192.168.0.246', 23, 'root', 'root123', 'ssh')
    #gvboxObj.openConnection()
    #print gvboxObj.sendCommand ('show snmp')
    #gvboxObj.closeConnection()
    #cOutput1 = hdboxObj.sendCommand ('show system', context='P_STANDARD', exp_prompt='GV\d+-\d+>')
    #print hdboxObj.sendCommand ('config system ssh2 1', context='P_EXIT')
    #time.sleep(30)
    ips = [
           '10.150.55.20',
           '10.150.55.21',
           '10.150.55.22',
           '10.150.55.23',
           '10.150.55.24',
           '10.150.55.25',
           '10.150.55.26',
           '10.150.55.27',
           '10.150.55.28',
           '10.150.55.29',
           '10.150.55.30',
           '10.150.55.31',
           '10.150.55.32',
           '10.150.55.33',
           '10.150.55.34',
           '10.150.55.35',
           
           ]
    #for ip in ips:
    hdboxObj = DevCon('10.150.55.100', 22, 'admin', 'admin123A!', 'ssh')
    hdboxObj.openConnection()
        
    #cliCmd = 'interface eth1 shutdown' 
    #print hdboxObj.sendCommand (cliCmd, 'P_CONFIG', exp_prompt="Enter 'YES' to confirm this shutdown:\s+")
    #output = hdboxObj.sendCommand ('YES')
    #print "<2"+output+"2>"
    
    #cliCmd = 'no interface eth1 shutdown' 
    #print hdboxObj.sendCommand (cliCmd, 'P_CONFIG')#, exp_prompt="Enter 'YES' to confirm this shutdown:\s+")
    #output = hdboxObj.sendCommand ('YES')
    #print "<2"+output+"2>"
    print hdboxObj.sendCommand ('sho terminal', 'P_CONFIG')
     
    cliCmd = 'show port stats port-list 3/1/x28' 
    output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
    print "<1"+output+"1>"
    
    cliCmd = 'no traffic all' 
    output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
    print "<1"+output+"1>"
    
    cliCmd = 'no interface eth1 shutdown' 
    print hdboxObj.sendCommand (cliCmd, 'P_CONFIG')#, exp_prompt="Enter 'YES' to confirm this shutdown:\s+")
    output = hdboxObj.sendCommand ('YES')
    print "<2"+output+"2>"
    hdboxObj.closeConnection()
    
#     cliCmd = 'reload' 
#     print hdboxObj.sendCommand (cliCmd, 'P_CONFIG', exp_prompt = 'save first')#, exp_prompt="Enter 'YES' to confirm this shutdown:\s+")
#     print hdboxObj.sendCommand ('yes', exp_prompt='Confirm reload')
#     print hdboxObj.sendCommand ('yes', exp_prompt='System shutdown')
#     hdboxObj.closeConnection()
    #print "<2"+output+"2>"
    
    
    cliCmd = 'show port params brief' 
    output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
    print "<1"+output+"1>"
    
    hdboxObj.closeConnection()
#      
#     cliCmd = 'banner login TC-12689' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<2"+output+"2>"
#     
#     cliCmd = 'show configuration files' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)
#     print "<3"+output+"3>"
#      
#     cliCmd = 'no hostname' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)
#     print "<1"+output+"1>"
#      
#     #cliCmd = 'hostname hd8-ccv2-frs-108'
#     cliCmd = 'hostname TC-12688' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG',idle=1)
#     print "<4"+output+"4>"
#      
#     cliCmd = 'hostname hd8-ccv2-frs-108'
#     #cliCmd = 'hostname TC-12688' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)
#     print "<5"+output+"5>"
#      
#     cliCmd = 'show snmp' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<4"+output+"4>"
#      
#     cliCmd = 'snmp-server user admin v3 enable' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<5"+output+"5>"
#      
#     cliCmd = 'show snmp user' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<6"+output+"6>"
#      
#     cliCmd = 'show snmp engineID' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<7"+output+"7>"
#      
#     cliCmd = 'snmp-server user admin v3 enable'  
#     commonlib.glog('info', 'Command: {0}'.format(cliCmd))
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG')
#     print "<8"+output+"8>"
# #    
#     #print hdboxObj.sendCommand('show snmp user')
#     print hdboxObj.sendCommand ('map alias CS_TEKELEK_MSS_1', context='P_CONFIG')
#   #comment Converted_from_CS_TEKELEK_MSS_rule_1
#     print hdboxObj.sendCommand ('to 9/4/g2,9/5/x8')
#     print hdboxObj.sendCommand ('exit')
# #   from 9/1/x10..x11
# #   rule add pass ipsrc 10.224.38.192 255.255.255.192
# #   rule add pass ipdst 10.224.63.0 255.255.255.0
# #   rule add pass ipsrc 10.224.39.0 255.255.255.0
# #   rule add pass ipsrc 10.224.171.128 255.255.255.192
# #   rule add pass ipsrc 10.224.171.192 255.255.255.192
# #   rule add pass ipdst 10.224.38.192 255.255.255.192
# #   rule add pass ipdst 10.224.39.0 255.255.255.0
# #   rule add pass ipdst 10.224.171.128 255.255.255.192
# #   rule add pass ipdst 10.224.171.192 255.255.255.192
# #   rule add pass ipsrc 10.224.63.0 255.255.255.0
# #   exit
# #     
#     hdboxObj.closeConnection()
#     hdboxObj.openConnection()
#     #print "<5"+hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)+"5>"
#     
#     
#     cliCmd = 'interface eth1 dhcp' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)
#     print "<6"+output+"6>" 
#     
#     cliCmd = 'sho interfaces eth1' 
#     output = hdboxObj.sendCommand (cliCmd, 'P_CONFIG', idle=1)
#     print "<7"+output+"7>"
# 
#     hdboxObj.closeConnection()

#    168print hdboxObj.sendCommand ('reload', context='P_ENABLE', exp_prompt='Configuration has been modified; save first\? \[\w+\]')
#    print hdboxObj.sendCommand ('no\r', exp_prompt='Confirm reload\? \[\w+\]')
#    print hdboxObj.sendCommand ('yes\r', exp_prompt='Rebooting')
#    #print hdboxObj.sendCommand ('show system')
#    #print hdboxObj.sendCommand ('gigatest\n')
#    hdboxObj.closeConnection()
    #time.sleep(90)
    #hdboxObj = DevCon('192.168.1.23', 23, 'root', 'root123', 'telnet')
    #hdboxObj.openConnection()
    #print hdboxObj.sendCommand ('show system')
    #cOutput3 = hdboxObj.sendCommand ('y', context='P_EXIT')
    #cOutput4 = hdboxObj.sendCommand ('y', context='P_EXIT')
    #cOutput5 = hdboxObj.sendCommand ('y', context='P_EXIT')

    #print cOutput2
    #if cOutput1.find(deviceParams2['ipaddress'][:-3]) >= 0:
#        print 'Test PASSED'
    #else:
    #    print 'Test FAILED'