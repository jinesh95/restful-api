import config,random



def json_tool_to_network(port):
    
    Json_tool_to_network = {
                        "gigaPorts":
                        [{"portId":str(port),
                          "portType":"network",
                          "adminStatus":random.choice(['enable','disable']),
                          "configSpeed":"1000",
                          "operSpeed":"1000",
                          "duplex":"full",
                          "autoNegEnabled":True,
                          "forceLinkUpEnabled": random.choice([True,False])}]}
    
    return Json_tool_to_network 


def json_network_to_tool(port):
    
    Json_network_to_tool = {
                        "gigaPorts":
                        [{"portId":str(port),
                          "portType":"tool",
                          "adminStatus":random.choice(['enable','disable']),
                          "configSpeed":"1000",
                          "operSpeed":"1000",
                          "duplex":"full",
                          "autoNegEnabled":True,
                          "forceLinkUpEnabled": random.choice([True,False])}]}
    return Json_network_to_tool


def json_port_admin(port,status):
    
    Json_Port_Admin = {
                       "gigaPorts":
                        [{"portId":str(port),
                          "portType":"tool",
                          "adminStatus":str(status),
                          "configSpeed":"1000",
                          "operSpeed":"1000",
                          "duplex":"full",
                          "autoNegEnabled":True,
                          "forceLinkUpEnabled": random.choice([True,False])}]}
    return Json_Port_Admin

def json_force_link(port,status):
    
    Json_Port_Link_Status = {
                             "gigaPorts":
                             [{"portId":str(port),
                               "portType":random.choice(['tool','network']),
                               "adminStatus":"enable",
                               "configSpeed":"10000",
                               "operSpeed":"1000",
                               "duplex":"full",
                               "autoNegEnabled":True,
                               "forceLinkUpEnabled":status}]}
    return Json_Port_Link_Status
                             
                            
    